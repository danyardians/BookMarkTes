package com.dany.android.tesbookmark;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SharedPreference {
    public static final String PREFS_NAME = "PRODUCT_APP";
    public static final String FAVORITES = "Product_Favorites";

    public SharedPreference(){
        super();
    }

    // This four method are used for maintaining favorites
    public void saveFavorites(Context context, List<Product> favorites){
        SharedPreferences setting;
        SharedPreferences.Editor editor;

        setting = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = setting.edit();

        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(favorites);

        editor.putString(FAVORITES, jsonFavorites);

        editor.commit();
    }

    public void addFavorite(Context context, Product product){
        List<Product> favorites = geFavorites(context);
        if (favorites == null)
            favorites = new ArrayList<Product>();
        favorites.add(product);
        saveFavorites(context, favorites);
    }

    public  void removeFavorite(Context context, Product product){
        ArrayList<Product> favorites = geFavorites(context);
        if (favorites != null){
            favorites.remove(product);
            saveFavorites(context, favorites);
        }
    }

    public ArrayList<Product> geFavorites(Context context) {
        SharedPreferences settings;
        List<Product> favorites;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(FAVORITES)){
            String jsonFavorites = settings.getString(FAVORITES, null);
            Gson gson = new Gson();
            Product[] favoriteItems = gson.fromJson(jsonFavorites,
                    Product[].class);

            favorites = Arrays.asList(favoriteItems);
            favorites = new ArrayList<Product>(favorites);
        } else
            return null;

        return (ArrayList<Product>) favorites;
    }
}
